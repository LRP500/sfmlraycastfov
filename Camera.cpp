//
// Created by Pierre Roy on 18/04/18.
//

#include <cmath>

#include "Camera.hpp"
#include "global.h"

Camera::Camera(Map& map)
        : m_map(map), m_radius(3), m_speedMove(200),
          m_fov(map, m_camera)
{
    m_camera.setRadius(m_radius);
    m_camera.setFillColor(sf::Color::White);
    m_camera.setOrigin(m_radius, m_radius);
    m_camera.setPosition(WIDTH / 2, HEIGHT / 2);
}

void Camera::update(sf::Time dt)
{
    m_fov.update(dt);
}

void Camera::handleInput(sf::Time dt)
{
    sf::Vector2f newPos = m_camera.getPosition();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        newPos.y -= m_speedMove * dt.asSeconds();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        newPos.y += m_speedMove * dt.asSeconds();
    }

    if (newPos.y > 0 && newPos.y < HEIGHT)
        m_camera.setPosition(newPos);

    newPos = m_camera.getPosition();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        newPos.x -= m_speedMove * dt.asSeconds();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        newPos.x += m_speedMove * dt.asSeconds();
    }

    if (newPos.x > 0 && newPos.x < WIDTH)
        m_camera.setPosition(newPos);
}

void Camera::draw(sf::RenderWindow& window)
{
    window.draw(m_camera);
    m_fov.draw(window);
}