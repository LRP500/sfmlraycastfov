//
// Created by Pierre Roy on 18/04/18.
//

#ifndef RAYSCASTSEGMENTS_CAMERA_HPP
#define RAYSCASTSEGMENTS_CAMERA_HPP

#include <SFML/Graphics.hpp>
#include "Map.hpp"
#include "FOV.hpp"

class Camera
{
private:
    sf::CircleShape m_camera;
    Map& m_map;
    FOV m_fov;

    float m_radius;
    float m_speedMove;

public:
    explicit Camera(Map& map);

    void update(sf::Time);
    void handleInput(sf::Time);
    void draw(sf::RenderWindow&);
};


#endif //RAYSCASTSEGMENTS_CAMERA_HPP
