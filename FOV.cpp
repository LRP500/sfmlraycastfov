//
// Created by Pierre Roy on 25/04/18.
//

#include <cmath>
#include "FOV.hpp"

FOV::FOV(Map& map, sf::Shape& origin) : m_map(map), m_origin(origin)
{}

void FOV::update(sf::Time dt)
{
    getAngles();
    intersect();
}

void FOV::handleInput(sf::Time)
{}

void FOV::draw(sf::RenderWindow &window)
{
    // draw debug reys & dots
    for (auto&& intersection : m_intersections)
    {
        sf::VertexArray ray(sf::LinesStrip, 2);
        ray[0].position = m_origin.getPosition();
        ray[1].position = intersection.position;
        ray[0].color = sf::Color::Red;
        ray[1].color = sf::Color::Red;
        //window.draw(ray);

        sf::CircleShape dot;
        dot.setRadius(2);
        dot.setPosition(ray[1].position);
        dot.setOrigin(dot.getRadius(), dot.getRadius());
        dot.setFillColor(sf::Color::Red);
        //window.draw(dot);
    }

    // draw polygons
    for (int i = 0; i < m_intersections.size(); ++i)
    {
        // get neighboor Rey
        Intersection neighboor = (i == m_intersections.size() - 1) ? m_intersections[0] : m_intersections[i + 1];

        // create polygon
        sf::ConvexShape polygon;
        polygon.setPointCount(3);
        polygon.setPoint(0, m_origin.getPosition());
        polygon.setPoint(1, m_intersections[i].position);
        polygon.setPoint(2, neighboor.position);
        polygon.setFillColor(sf::Color(255, 0, 0, 150));
        window.draw(polygon);
    }
}

void FOV::getAngles()
{
    // store all angles to raycast at
    m_angles.clear();
    for (auto&& point : m_map.getPoints())
    {
        float angle = std::atan2(point.y - m_origin.getPosition().y,
                                 point.x - m_origin.getPosition().x);
        m_angles.emplace_back(angle);
        // adding +/- 0.00001 rad angles to check beyond edges (increase value if flicker)
        m_angles.emplace_back(angle - ANGLE_MARGIN);
        m_angles.emplace_back(angle + ANGLE_MARGIN);
    }
}

void FOV::intersect()
{
    Raycaster rc;
    m_intersections.clear();
    for (auto &&angle : m_angles) {
        // Calculate dx & dy from angle
        float dx = std::cos(angle);
        float dy = std::sin(angle);

        // Ray from camera pos to new pos
        sf::VertexArray ray(sf::LinesStrip, 2);
        ray[0].position = sf::Vector2f(m_origin.getPosition().x, m_origin.getPosition().y);
        ray[1].position = sf::Vector2f(m_origin.getPosition().x + dx, m_origin.getPosition().y + dy);

        // Find CLOSEST intersection
        bool first = true;
        Intersection closestIntersect;
        for (const auto &segment : m_map.getSegments())
        {
            auto intersection = rc.cast(ray, segment);
            if (!intersection.status) continue;
            if (first || intersection.distance < closestIntersect.distance)
            {
                closestIntersect = intersection;
            }
            first = false;
        }

        closestIntersect.angle = angle; // store angle for sort comparison
        m_intersections.emplace_back(closestIntersect); // Add to list of intersects
    }
    sortIntersections();
}

// sort intersections depending on ray angle
void FOV::sortIntersections()
{
    std::sort(m_intersections.begin(),
              m_intersections.end(),
              [](Intersection a, Intersection b)
              {
                  return a.angle > b.angle;
              });
}