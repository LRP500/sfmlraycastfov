//
// Created by Pierre Roy on 25/04/18.
//

#ifndef SFMLRAYCASTFOV_FOV_HPP
#define SFMLRAYCASTFOV_FOV_HPP

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <vector>

#include "Raycaster.hpp"
#include "Map.hpp"

using namespace Raycast;

class FOV
{
private:
    static constexpr float ANGLE_MARGIN = 0.0001f;

private:
    Map& m_map;
    sf::Shape& m_origin;

    std::vector<float> m_angles; // all angles to raycast at
    std::vector<Intersection> m_intersections; // raycast resulting intersections

public:
    FOV(Map&, sf::Shape& origin);

    void update(sf::Time);
    void handleInput(sf::Time);
    void draw(sf::RenderWindow&);

    void getAngles();
    void intersect();

    void sortIntersections();
};


#endif //SFMLRAYCASTFOV_FOV_HPP
