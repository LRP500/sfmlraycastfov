//
// Created by Pierre Roy on 20/04/18.
//

#include <iostream>
#include "Map.hpp"
#include "global.h"

Map::Map()
{
    m_shapes.reserve(6);

    sf::ConvexShape borders;
    borders.setPointCount(4);
    borders.setPoint(0, sf::Vector2f(0, 0));
    borders.setPoint(1, sf::Vector2f(WIDTH, 0));
    borders.setPoint(2, sf::Vector2f(WIDTH, HEIGHT));
    borders.setPoint(3, sf::Vector2f(0, HEIGHT));
    borders.setFillColor(LIGHT_GREY);
    m_shapes.emplace_back(borders);

    sf::ConvexShape shape;
    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(600, 100));
    shape.setPoint(1, sf::Vector2f(500, 300));
    shape.setPoint(2, sf::Vector2f(700, 400));
    shape.setFillColor(DARK_GREY);
    m_shapes.emplace_back(shape);

    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(1200, 100));
    shape.setPoint(1, sf::Vector2f(1000, 300));
    shape.setPoint(2, sf::Vector2f(1400, 400));
    shape.setFillColor(DARK_GREY);
    m_shapes.emplace_back(shape);

    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(1000, 600));
    shape.setPoint(1, sf::Vector2f(800, 700));
    shape.setPoint(2, sf::Vector2f(1000, 800));
    shape.setFillColor(DARK_GREY);
    m_shapes.emplace_back(shape);

    shape.setPointCount(3);
    shape.setPoint(0, sf::Vector2f(500, 550));
    shape.setPoint(1, sf::Vector2f(400, 620));
    shape.setPoint(2, sf::Vector2f(500, 650));
    shape.setFillColor(DARK_GREY);
    m_shapes.emplace_back(shape);

    // store all segments & unique points
    for (auto&& s : m_shapes)
    {
        for (size_t i = 0; i < s.getPointCount(); ++i)
        {
            // unique points
            m_points.emplace_back(s.getPoint(static_cast<size_t>(i)));

            // segments
            sf::VertexArray seg(sf::LinesStrip, 2);
            seg[0].position = s.getPoint(i);
            seg[1].position = (i + 1 < s.getPointCount()) ? s.getPoint(i + 1) : s.getPoint(0);
            seg[0].color = sf::Color::Red;
            seg[1].color = sf::Color::Red;
            m_segments.emplace_back(seg);
        }
    }
}

void Map::draw(sf::RenderWindow& window)
{
    for (auto&& shape : m_shapes)
    {
        window.draw(shape);
    }
}