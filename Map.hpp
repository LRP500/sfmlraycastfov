//
// Created by Pierre Roy on 20/04/18.
//

#ifndef RAYSCASTSEGMENTS_MAP_HPP
#define RAYSCASTSEGMENTS_MAP_HPP

#include <SFML/Graphics.hpp>

class Map
{
private:
    std::vector<sf::ConvexShape> m_shapes;
    std::vector<sf::VertexArray> m_segments;
    std::vector<sf::Vector2f> m_points; // segment points

public:
    Map();

    void draw(sf::RenderWindow&);

    const std::vector<sf::Vector2f>& getPoints() const { return m_points; }
    const std::vector<sf::ConvexShape>& getShapes() const { return m_shapes; }
    const std::vector<sf::VertexArray>& getSegments() const { return m_segments; }
};


#endif //RAYSCASTSEGMENTS_MAP_HPP
