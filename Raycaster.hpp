//
// Created by Pierre Roy on 25/04/18.
//

#ifndef SFMLRAYCASTFOV_RAYCASTER_HPP
#define SFMLRAYCASTFOV_RAYCASTER_HPP

#include <SFML/Graphics/VertexArray.hpp>

namespace Raycast
{
    struct Intersection
    {
        sf::Vector2f position = sf::Vector2f();
        float distance = 0.0f;
        float angle = 0.0f;
        float status = false;
    };

    class Raycaster
    {
    private:
        Intersection m_result;

    public:
        Raycaster() = default;

        Intersection cast(const sf::VertexArray&, const sf::VertexArray&);
        void reset();
    };
}


#endif //SFMLRAYCASTFOV_RAYCASTER_HPP
