//
// Created by Pierre Roy on 09/04/18.
//

#ifndef RAYCAST_GLOBAL_H
#define RAYCAST_GLOBAL_H

#include <SFML/Graphics/Color.hpp>

const unsigned int WIDTH = 1600;
const unsigned int HEIGHT = 900;

const sf::Color LIGHT_GREY(110, 110, 110, 255);
const sf::Color GREY(70, 70, 70, 255);
const sf::Color DARK_GREY(47, 47, 47, 255);
const sf::Color ORANGE(249, 158, 20, 255);
const sf::Color YELLOW(255, 215, 0, 255);

#endif //RAYCAST_GLOBAL_H
