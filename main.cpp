#include <iostream>
#include <SFML/Graphics.hpp>

#include "Camera.hpp"
#include "global.h"

int main()
{
    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFMLRaycastFOV", sf::Style::Titlebar);
    sf::Clock clock;

    Map map;
    Camera camera(map);

    while (window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        sf::Event event {};

        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Escape: window.close(); break;
                    }
                case sf::Event::KeyPressed:
                    switch (event.key.code)
                    {
                        case sf::Keyboard::Escape: window.close(); break;
                    }
            }
        }

        camera.handleInput(elapsed);
        camera.update(elapsed);

        window.clear(sf::Color::Black);

        map.draw(window);
        camera.draw(window);

        window.display();
    }

    return EXIT_SUCCESS;
}